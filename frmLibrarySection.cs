﻿using Encrypt.ZFEA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZoconFEA3D;
//using ZDraw;

namespace CBridge
{
    public partial class frmLibrarySection : Form
    {
        string FileName;
        double[] dims;
        Kanvas2D m_View;
        public CrossSection[] SectionData = null;
        public CrossSection STemp = new CrossSection();
        public bool m_OK = false; // false=return from cancel, true from OK
        public frmLibrarySection()
        {
            InitializeComponent();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            m_OK = false;
            this.Close();
            return;
        }

        private void frmLibrarySection_Load(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reload();
            if (Properties.Settings.Default.savAddress == "" )
            {
                if (System.IO.File.Exists(System.IO.Directory.GetCurrentDirectory() + @"\Default.cbt"))
                {
                    txtBrowse.Text = System.IO.Directory.GetParent(Application.ExecutablePath) + "\\Default.cbt";
                    FileName = System.IO.Directory.GetParent(Application.ExecutablePath) + "\\Default.cbt";
                    ReadFile();
                }
                return;
            }
            else
            {
                txtBrowse.Text = Properties.Settings.Default.savAddress;
                FileName = txtBrowse.Text ;
                ReadFile();
                btnOK.Enabled = true;
                cmbSelType.Enabled = true;
            }
            //cmbSelType.Text = "Rectangle";
        }

        public void MatrixOperations_addRow(string[] colsep, ref string[,] ar, int row)
        {
            int ncol1 = 0;
            int ncol2 = 0;
            int ncol = 0;
            int nrow = 0;
            ncol2 = ar.GetUpperBound(1);
            ncol1 = colsep.GetUpperBound(0);
            ncol = Math.Max(ncol1, ncol2);
            nrow = ar.GetUpperBound(0);
            string[,] ar2 = null;
            ar2 = new string[nrow + 2, ncol + 1];
            for (var i = 0; i < row; i++)
            {
                for (var j = 0; j <= ncol2; j++)
                {
                    ar2[i, j] = ar[i, j];
                }
            }
            for (var j = 0; j <= ncol1; j++)
            {
                ar2[row, j] = colsep[j];
            }
            for (var i = row + 1; i <= nrow + 1; i++)
            {
                for (var j = 0; j <= ncol2; j++)
                {
                    ar2[i, j] = ar[i - 1, j];
                }
            }
            ar = ar2;
        }

        public string[,] StringTo2DArray2(string s, string delimiter, string EOLDelimiter)
        {

            char[] sep = new char[1];
            sep[0] = EOLDelimiter[0];
            string[] lines = s.Split(sep[0]);

            string[,] ar = new string[0, 0];
            int row = 0;
            while (row < lines.Length - 1)
            {
                sep[0] = delimiter[0];
                string thisLine = lines[row].TrimStart();
                string[] colsep = thisLine.Split(sep, StringSplitOptions.None);
                MatrixOperations.addRow(colsep,ref ar, row);
                row = row + 1;
            }
            return ar;
        }

    private void SetSectionText(string xs)
    {
        double[] secdata = null;
        string Sectype = null;
        string SecID = null;
        string SecUnit = null;
        bool hasSlab = false;
        double[] Slab = null;
        int ist = 0;

        string delimiter = "~";
        string EOLdelimiter = "%";
        string[,] table = StringTo2DArray2(xs, delimiter, EOLdelimiter);
        int nrow = table.GetLength(0);
        int jRow = 0;
        SectionData = new CrossSection[nrow];

        for (var i = jRow; i < jRow + nrow; i++)
        {
            var ii = i - jRow;

            SectionData[ii] = new ZoconFEA3D.CrossSection();

            Sectype = table[i, 0];
            SecID = table[i, 1];
            SecUnit = table[i, 2];
            ist = 3;
            if (Sectype == "Rectangle")
            {
                secdata = new double[3];
                for (var j = 0; j <= 2; j++)
                {
                    secdata[j] = Convert.ToDouble(table[i, j + ist]);
                }
                ist = ist + 3;
                if (table[i, ist] == "Yes")
                {
                    ist = ist + 1;
                    hasSlab = true;
                    Slab = new double[6];
                    for (var j = 0; j <= 5; j++)
                    {
                        Slab[j] = Convert.ToDouble(table[i, j + ist]);
                    }
                    ist = ist + 4;
                }
                else
                {
                    ist = ist + 1;
                    hasSlab = false;
                    Slab = null;
                }
                SectionData[ii].CrossSecName = SecID;
                SectionData[ii].SetSectionShape("Rectangle", secdata, hasSlab, Slab, true);
            }

            if (Sectype == "Circle")
            {
                secdata = new double[3];
                for (var j = 0; j <= 1; j++)
                {
                    secdata[j] = Convert.ToDouble(table[i, j + ist]);
                }
                ist = ist + 3;
                if (table[i, ist] == "Yes")
                {
                    ist = ist + 1;
                    hasSlab = true;
                    Slab = new double[6];
                    for (var j = 0; j <= 4; j++)
                    {
                        Slab[j] = Convert.ToDouble(table[i, j + ist]);
                    }
                    ist = ist + 4;
                }
                else
                {
                    ist = ist + 1;
                    hasSlab = false;
                    Slab = null;
                }
                SectionData[ii].CrossSecName = SecID;
                SectionData[ii].SetSectionShape("Circle", secdata, hasSlab, Slab, true);
            }

            if (Sectype == "I-Girder")
            {
                secdata = new double[11];
                for (var j = 0; j <= 10; j++)
                {
                    secdata[j] = Convert.ToDouble(table[i, j + ist]);
                }
                ist = ist + 11;
                if (table[i, ist] == "Yes")
                {
                    ist = ist + 1;
                    hasSlab = true;
                    Slab = new double[6];
                    for (var j = 0; j <= 5; j++)
                    {
                        Slab[j] = Convert.ToDouble(table[i, j + ist]);
                    }
                    ist = ist + 4;
                }
                else
                {
                    ist = ist + 1;
                    hasSlab = false;
                    Slab = null;
                }
                SectionData[ii].CrossSecName = SecID;

                SectionData[ii].SetSectionShape("I_Girder", secdata, hasSlab, Slab, true);
            }

            if (Sectype == "Box Girder")
            {
                secdata = new double[21];
                for (var j = 0; j <= 20; j++)
                {
                    secdata[j] = Convert.ToDouble(table[i, j + ist]);
                }
                ist = ist + 3;
                if (table[i, ist] == "Yes")
                {
                    ist = ist + 1;
                    hasSlab = true;
                    Slab = new double[6];
                    for (var j = 0; j <= 5; j++)
                    {
                        Slab[j] = Convert.ToDouble(table[i, j + ist]);
                    }
                    ist = ist + 4;
                }
                else
                {
                    ist = ist + 1;
                    hasSlab = false;
                    Slab = null;
                }
                SectionData[ii].CrossSecName = SecID;

                SectionData[ii].SetSectionShape("Box_Girder", secdata, hasSlab, Slab, true);
            }

            if (Sectype == "U-Girder")
            {
                secdata = new double[10];
                for (var j = 0; j <= 9; j++)
                {
                    secdata[j] = Convert.ToDouble(table[i, j + ist]);
                }
                ist = ist + 10;
                if (table[i, ist] == "Yes")
                {
                    ist = ist + 1;
                    hasSlab = true;
                    Slab = new double[6];
                    for (var j = 0; j <= 5; j++)
                    {
                        Slab[j] = Convert.ToDouble(table[i, j + ist]);
                    }
                    ist = ist + 4;
                }
                else
                {
                    ist = ist + 1;
                    hasSlab = false;
                    Slab = null;
                }
                SectionData[ii].CrossSecName = SecID;

                SectionData[ii].SetSectionShape("U_Girder", secdata, hasSlab, Slab, true);
            }

            if (Sectype == "Precast Box")
            {
                secdata = new double[7];
                for (var j = 0; j <= 6; j++)
                {
                    secdata[j] = Convert.ToDouble(table[i, j + ist]);
                }
                ist = ist + 7;
                if (table[i, ist] == "Yes")
                {
                    ist = ist + 1;
                    hasSlab = true;
                    Slab = new double[6];
                    for (var j = 0; j <= 5; j++)
                    {
                        Slab[j] = Convert.ToDouble(table[i, j + ist]);
                    }
                    ist = ist + 4;
                }
                else
                {
                    ist = ist + 1;
                    hasSlab = false;
                    Slab = null;
                }
                SectionData[ii].CrossSecName = SecID;

                SectionData[ii].SetSectionShape("PC_Box", secdata, hasSlab, Slab, true);
            }

            if (Sectype == "Voided Slab")
            {
                secdata = new double[21];
                for (var j = 0; j <= 20; j++)
                {
                    secdata[j] = Convert.ToDouble(table[i, j + ist]);
                }
                ist = ist + 3;
                if (table[i, ist] == "Yes")
                {
                    ist = ist + 1;
                    hasSlab = true;
                    Slab = new double[6];
                    for (var j = 0; j <= 5; j++)
                    {
                        Slab[j] = Convert.ToDouble(table[i, j + ist]);
                    }
                    ist = ist + 4;
                }
                else
                {
                    ist = ist + 1;
                    hasSlab = false;
                    Slab = null;
                }
                SectionData[ii].CrossSecName = SecID;

                SectionData[ii].SetSectionShape("VoidedSlab", secdata, hasSlab, Slab, true);
            }

            if (Sectype == "CA BathTub")
            {
                secdata = new double[12];
                for (var j = 0; j <= 11; j++)
                {
                    secdata[j] = Convert.ToDouble(table[i, j + ist]);
                }
                ist = ist + 3;
                if (table[i, ist] == "Yes")
                {
                    ist = ist + 1;
                    hasSlab = true;
                    Slab = new double[6];
                    for (var j = 0; j <= 5; j++)
                    {
                        Slab[j] = Convert.ToDouble(table[i, j + ist]);
                    }
                    ist = ist + 4;
                }
                else
                {
                    ist = ist + 1;
                    hasSlab = false;
                    Slab = null;
                }
                SectionData[ii].CrossSecName = SecID;

                SectionData[ii].SetSectionShape("CA BathTub", secdata, hasSlab, Slab, true);
            }

            if (Sectype == "Parts")
            {
                int nsize = 0;
addrows:
                if (table[i, nsize + ist + 1] != "No")
                {
                    nsize = nsize + Convert.ToInt32(table[i, nsize + ist + 1]) + 1;
                    goto addrows;
                }
                secdata = new double[nsize + 1];
                for (var j = 0; j <= nsize; j++)
                {
                    secdata[j] = Convert.ToDouble(table[i, j + ist]);
                }
                ist = ist + 3;
                if (table[i, ist] == "Yes")
                {
                    ist = ist + 1;
                    hasSlab = true;
                    Slab = new double[6];
                    for (var j = 0; j <= 5; j++)
                    {
                        Slab[j] = Convert.ToDouble(table[i, j + ist]);
                    }
                    ist = ist + 4;
                }
                else
                {
                    ist = ist + 1;
                    hasSlab = false;
                    Slab = null;
                }
                SectionData[ii].CrossSecName = SecID;

                SectionData[ii].SetSectionShape("Parts", secdata, hasSlab, Slab, true);
            }

        }
    }

    private void ReadFile()
    {
        txtBrowse.Text = FileName;
        vbEncrypt C = new vbEncrypt();
        string Data = C.GetFile(FileName);
        SetSectionText(Data);

        int nrows = SectionData.GetUpperBound(0);

        lstBoxViewID.Items.Clear();
        if (SectionData != null && SectionData.Length > 1)
        {
            string SecShape = SectionData[1].SecShape();
            if (SecShape == "Rectangle") cmbSelType.SelectedIndex = 0;
            else if
            (SecShape == "Circle") cmbSelType.SelectedIndex = 1;
            else if
            (SecShape == "I_Girder") cmbSelType.SelectedIndex = 2;
            else if
            (SecShape == "Box_Girder") cmbSelType.SelectedIndex = 3;
            else if
            (SecShape == "U_Girder") cmbSelType.SelectedIndex = 4;
            else if
            (SecShape == "PC_Box") cmbSelType.SelectedIndex = 5;

            lstBoxViewID.Items.Clear();

            for (int i = 0; i <= nrows; i++)
            {
                if (SectionData[i].SecShape() == SecShape)
                {
                    lstBoxViewID.Items.Add(SectionData[i].CrossSecName);
                }
            }
            if (lstBoxViewID.Items.Count > 0) lstBoxViewID.SelectedIndex = 0;
        }
        btnOK.Enabled = true;
        cmbSelType.Enabled = true;
        return;
    }

    private void btnBrowse_Click(object sender, EventArgs e)
    {
        ofdOpen.FileName = "";
        if (ofdOpen.ShowDialog() == DialogResult.OK)
        {
            FileName = ofdOpen.FileName;
            ReadFile();
        }
    }
            private void cmbSelType_SelectedIndexChanged(object sender, EventArgs e)
            {
                string Shape = "";
                switch (cmbSelType.Text)
                {
                    case "Rectangle":
                        Shape = "Rectangle";
                        break;
                    case "Circle":
                        Shape = "Circle";
                        break;
                    case "I-Girder":
                        Shape = "I_Girder";
                        break;
                    case "Box Girder":
                        Shape = "Box_Girder";
                        break;
                    case "U-Girder":
                        Shape = "U_Girder";
                        break;
                    case "Precast Box":
                        Shape = "PC_Box";
                        break;
                }

                if (Shape != "")
                {
                    int nrows = SectionData.GetUpperBound(0);

                    lstBoxViewID.Items.Clear();

                    for (int i = 0; i <= nrows; i++)
                    {
                        if (SectionData[i].SecShape() == Shape)
                        {
                            lstBoxViewID.Items.Add(SectionData[i].CrossSecName);
                        }
                    }
                    if (lstBoxViewID.Items.Count > 0) lstBoxViewID.SelectedIndex = 0;
                }

                if (lstBoxViewID.Items.Count < 1)
                {
                    btnOK.Enabled = false;
                    return;
                }
                else 
                {
                    btnOK.Enabled = true;
                }
            }

            private void btnCancel_Click(object sender, EventArgs e)
            {
                this.Close();
            }

            private void lstBoxViewID_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (lstBoxViewID.SelectedItem == null) return;
                m_View = new Kanvas2D();
                m_View.SetWinSize(pbModel.Size.Width, pbModel.Size.Height);
                m_View.AddPen(new Pen(Color.Green));
                string SelectedID = lstBoxViewID.SelectedItem.ToString();

                switch (cmbSelType.SelectedIndex)
                {
                    case 0:
                    for (int i = 0; i < SectionData.GetUpperBound(0); i++)
                    {
                        if (SectionData[i].CrossSecName == SelectedID)
                        {
                            STemp = SectionData[i].Duplicate();
                            dims = STemp.getShapeDims();
                            dims[0] = dims[1];
                            dims[1] = dims[2];
                            double[,] XY;
                            XY = STemp.SecPointsByShape("Rectangle", dims, false, null);
                            m_View.AddPolygon(XY, 1, 0);
                            m_View.ZoomFit();
                            pbModel.Image = m_View.GetPicture();
                        }
                    }
                    break;

                    case 1:
                    for (int i = 0; i < SectionData.GetUpperBound(0); i++)
                    {
                        if (SectionData[i].CrossSecName == SelectedID)
                        {
                            STemp = SectionData[i].Duplicate();
                            dims = STemp.getShapeDims();
                            dims[0] = dims[1];
                            double[,] XY;
                            XY = STemp.SecPointsByShape("Circle", dims, false, null);
                            m_View.AddPolygon(XY, 1, 0);
                            m_View.ZoomFit();
                            pbModel.Image = m_View.GetPicture();
                        }
                    }
                    break;

                    case 2:
                    for (int i = 0; i < SectionData.GetUpperBound(0); i++)
                    {
                        if (SectionData[i].CrossSecName == SelectedID)
                        {
                            STemp = SectionData[i].Duplicate();
                            dims = STemp.getShapeDims();
                            dims[0] = dims[1];
                            dims[1] = dims[2];
                            dims[2] = dims[3];
                            dims[3] = dims[4];
                            dims[4] = dims[5];
                            dims[5] = dims[6];
                            dims[6] = dims[7];
                            dims[7] = dims[8];
                            dims[8] = dims[9];
                            dims[9] = dims[10];
                            double[,] XY;
                            XY = STemp.SecPointsByShape("I_Girder", dims, false, null);
                            m_View.AddPolygon(XY, 1, 0);
                            m_View.ZoomFit();
                            pbModel.Image = m_View.GetPicture();
                        }
                    }
                    break;

                    case 3:
                    for (int i = 0; i < SectionData.GetUpperBound(0); i++)
                    {
                        if (SectionData[i].CrossSecName == SelectedID)
                        {
                            STemp = SectionData[i].Duplicate();
                            dims = STemp.getShapeDims();
                            dims[0] = dims[1];
                            dims[1] = dims[2];
                            dims[2] = dims[3];
                            dims[3] = dims[4];
                            dims[4] = dims[5];
                            dims[5] = dims[6];
                            dims[6] = dims[7];
                            dims[7] = dims[8];
                            dims[8] = dims[9];
                            dims[9] = dims[10];
                            dims[10] = dims[11];
                            dims[11] = dims[12];
                            dims[12] = dims[13];
                            dims[13] = dims[14];
                            dims[14] = dims[15];
                            dims[15] = dims[16];
                            dims[16] = dims[17];
                            dims[17] = dims[18];
                            dims[18] = dims[19];
                            dims[19] = dims[20];
                            double[,] XY;
                            XY = STemp.SecPointsByShape("Box_Girder", dims, false, null);
                            m_View.AddPolygon(XY, 1, 0);
                            m_View.ZoomFit();
                            pbModel.Image = m_View.GetPicture();
                        }
                    }
                    break;

                    case 4:
                    for (int i = 0; i < SectionData.GetUpperBound(0); i++)
                    {
                        if (SectionData[i].CrossSecName == SelectedID)
                        {
                            STemp = SectionData[i].Duplicate();
                            dims = STemp.getShapeDims();
                            dims[0] = dims[1];
                            dims[1] = dims[2];
                            dims[2] = dims[3];
                            dims[3] = dims[4];
                            dims[4] = dims[5];
                            dims[5] = dims[6];
                            dims[6] = dims[7];
                            dims[7] = dims[8];
                            dims[8] = dims[9];
                            double[,] XY;
                            XY = STemp.SecPointsByShape("U_Girder", dims, false, null);
                            m_View.AddPolygon(XY, 1, 0);
                            m_View.ZoomFit();
                            pbModel.Image = m_View.GetPicture();
                        }
                    }
                    break;

                    case 5:
                    for (int i = 0; i < SectionData.GetUpperBound(0); i++)
                    {
                        if (SectionData[i].CrossSecName == SelectedID)
                        {
                            STemp = SectionData[i].Duplicate();
                            dims = STemp.getShapeDims();
                            dims[0] = dims[1];
                            dims[1] = dims[2];
                            dims[2] = dims[3];
                            dims[3] = dims[4];
                            dims[4] = dims[5];
                            dims[5] = dims[6];
                            double[,] XY;
                            XY = STemp.SecPointsByShape("PC_Box", dims, false, null);
                            m_View.AddPolygon(XY, 1, 0);
                            m_View.ZoomFit();
                            pbModel.Image = m_View.GetPicture();
                        }
                    }
                    break;
                }
              }

            private void btnOK_Click(object sender, EventArgs e)
            {
                string SelectedID = lstBoxViewID.SelectedItem.ToString();

                for (int i = 0; i < SectionData.GetUpperBound(0); i++)
                {
                    if (SectionData[i].CrossSecName == SelectedID)
                    {
                        double[] SecData = SectionData[i].getShapeDims();
                        double[] SecDataNew = new double[SecData.Length - 1];
                        for (int j = 0; j < SecData.Length-1; j++) SecDataNew[j] = SecData[j + 1];
                        STemp = SectionData[i].Duplicate();
                        string Shape = STemp.SecShape();
                        STemp.SetSectionShape(Shape,SecDataNew,false,null,true);

                        Properties.Settings.Default.savAddress = txtBrowse.Text;
                        Properties.Settings.Default.Save();

                        m_OK = true;
                        Close();
                        return;
                    }
                }
                m_OK = true;
                this.Close();
                return;
            }
        }
    }

